---
format:
  html:
    toc: true
    toc-depth: 3
    toc-location: right
    number-sections: False
    number-depth: 3
    html-math-method: katex
    css: /styles.css
    self-contained: false
    anchor-sections: true
    smooth-scroll: true
execute:
  echo: false
---

::: {.justify .content-visible when-profile="french"}

# Position topographique

## Entrées 

Cet outil demande en entrées les trois couches constituant les "objets" haies issues du module *1 - Préparation des données* ou *2 - Transformation des données* ainsi qu'un Modèle Numérique de Terrain (MNT).

![](/img/geographic_context/topographic_position/interface.png){fig-align="center" width=65%}

## Paramètres

Cet algorithme se base sur les géomorphons de GRASS GIS. Pour d'avantage de précision vous pouvez consulter la page d'aide de l'outil [r.geomorphon](https://grass.osgeo.org/grass83/manuals/r.geomorphon.html).

#### Distance de recherche 

Ce paramètre régit la distance, en mètres, à laquelle l'algorithme va regarder dans les 8 directions afin de déterminer la forme du terrain.
Une valeur plus importante prendra en compte des formes plus importantes et la valeur devrait être adaptée à l'échelle de votre terrain.

#### Distance à ignorer 

Ce paramètre permet d'ignorer les micro-topographies sur la distance renseignée en mètres. Une plus grande valeur ignorera d'avantage les formes proches.
La valeur de ce paramètre devrait toujours être inférieure à la valeur de la distance de recherche. 

#### Filtre médian 

Ce paramètre prend une valeur impaire renseignant la taille de la fenêtre mouvante en nombre de pixels.
Il permet d'ignorer d'avantage les faibles reliefs du MNT.
La valeur par défaut est adaptée à un MNT de résolution de 5 mètres et devrait être augmentée si un MNT de plus haute résolution est utilisé.

## Sorties

La position topographique de la haie est attribuée par le géomorphon qui possède le plus haut recouvrement avec son polygone.
L'outil crée un champ **topo_pos** dans la table attributaire de la couche surfacique.
Il est fait le choix de garder les 10 formes de terrain possibles, à savoir : plat, sommet, crête, haut de pente, éperon, pente, vallon/creux, bas de pente, vallée, trou.
L'utilisateur peut cependant choisir de regrouper les formes intermédiaires selon ses besoins.

![Table des polygones](/img/geographic_context/topographic_position/output.png){fig-align="center"}
:::

::: {.justify .content-visible when-profile="english"}

# Topographic position

## Inputs

This tool requires three layers representing hedge "objects" from the *1 - Data Preparation* or *2 - Data Transformation* modules, along with a Digital Terrain Model (DTM).

![](/img/geographic_context/topographic_position/interface.png){fig-align="center" width=65%}

## Parameters

This algorithm is based on GRASS GIS's geomorphons. For more precision, you can consult the tool's help page [r.geomorphon](https://grass.osgeo.org/grass83/manuals/r.geomorphon.html).

#### Search Distance 

This parameter governs the distance, in meters, in which the algorithm will look in 8 directions to determine the terrain's form. A larger value will consider more extensive shapes and should be adapted to the scale of your terrain.

#### Ignoring Distance 

This parameter allows ignoring micro-topographies within the specified distance in meters. A larger value will ignore closer shapes more effectively. This parameter value should always be less than the search distance.

#### Median Filter 

This parameter takes an odd value specifying the size of the moving window in the number of pixels. It allows further ignoring the minor relief in the DTM. The default value is suitable for a 5-meter resolution DTM and should be increased if using a higher-resolution DTM.

## Outputs

The hedge's topographic position is assigned based on the geomorphon with the highest overlap with its polygon. The tool creates a **topo_pos** field in the surface layer's attribute table.

It retains the 10 possible terrain forms: flat, peak, ridge, shoulder, spur, slope, hollow, footslope, valley, pit. However, the user can choose to group intermediate forms as needed.

![Polygons table](/img/geographic_context/topographic_position/output.png){fig-align="center"}

:::
