---
format:
  html:
    toc: true
    toc-depth: 3
    toc-location: right
    number-sections: False
    number-depth: 3
    html-math-method: katex
    css: /styles.css
    self-contained: false
    anchor-sections: true
    smooth-scroll: true
execute:
  echo: false
---

::: {.justify .content-visible when-profile="french"}

# 6 - Découpage par un réseau

## Entrées 

Cet outil permet de découper les arbres isolés et les linéaires arborés et arbustifs à partir de couche linéaire représentant un réseau (rivière, route, ...) afin d'en distinguer les haies doubles. 

![](/img/extraction/split_by_network/interface.png){fig-align="center" width=65%}

#### Couches vecteur

*Hedge layer* et *Tree layer* permettent de renseigner les couches, respectivement, des linéaires arborés et arbustifs et des arbres isolés. L'utilisateur peut choisir de ne renseigner que l'une ou l'autre ou les deux.

*Network layers* renseigne toutes les couches linéaires servant à découper les polygones.

## Paramètres

#### Zone tampon pour les réseaux

*Buffer value around the networks geometries* désigne la valeur en mètres à appliquer aux couches de réseau. Les entités découpées seront séparés par le double de cette valeur.

::: columns
::: {.column width="10%"}

![](/img/misc/warning.png){fig-align="left"}

:::

::: {.column width="2%"}

:::

::: {.column width="88%"}

Cette valeur devra être renseignée dans l'outil [agrégation](aggregation.html).

:::
:::

#### Taille maximum des arbres 

*Tree area threshold*  permets de transférer les géométries découpées en provenance de la couche des linéaires arborés et arbustifs à la couche des arbres isolés.

La suppression des petites entités est prioritaire si elle est activée.

#### Suppression des petites entités

*Minimum area of a geometry* supprime les résidus issues du découpage par le réseau si ils sont en dessous de la valeur en mètres carrés renseignées. *Delete geometries below minimum area value* doit être coché pour que cette suppression soit effective.
 
## Sorties

*Splitted hedges* et *Splitted trees* indiquent respectivement les couches des linéaires arborés et arbustifs et des arbres isolés découpés par le réseau.

:::

::: {.justify .content-visible when-profile="english"}

# 6 - Split by network

## Inputs 

This tool allows for the segmentation of isolated trees and arboreal linear features based on a linear layer representing a network (such as rivers, roads, etc.) allowing for the distinction of double hedges.

![](/img/extraction/split_by_network/interface.png){fig-align="center" width=65%}

#### Vector layers

*Hedge layer* and *Tree layer* are used to specify the layers for arboreal linear features and isolated trees, respectively. The user can choose to input only one of them, both, or neither.

*Network layers* specifies all the linear layers used to segment the polygons.

## Parameters

#### Buffer zone for networks

*Buffer value around the networks geometries* refers to the value in meters applied to the network layers. The segmented entities will be separated by twice this value.


::: columns
::: {.column width="10%"}

![](/img/misc/warning.png){fig-align="left"}

:::

::: {.column width="2%"}

:::

::: {.column width="88%"}

This value must be specified in the [aggregation](aggregation.html) tool.

:::
:::


#### Maximum tree size

*Tree area threshold* allows transferring the segmented geometries from the layer of arboreal linear features to the layer of isolated trees.

The removal of small entities takes priority if activated.

#### Removal of small entities

*Minimum area of a geometry* deletes residues resulting from the network segmentation if they are below the specified area value in square meters. *Delete geometries below minimum area value* must be checked for this removal to take effect.

## Outputs

*Splitted hedges* and *Splitted trees* indicate the layers of segmented arboreal linear features and isolated trees, respectively, as segmented by the network.

:::