---
format:
  html:
    toc: true
    toc-depth: 3
    toc-location: right
    number-sections: False
    number-depth: 3
    html-math-method: katex
    css: /styles.css
    self-contained: false
    anchor-sections: true
    smooth-scroll: true
execute:
  echo: false
---

::: {.justify .content-visible when-profile="french"}

# Création des sous-réseaux

## Entrées 

Cet outil prend la couche de linéaire et la couche de ponctuelle et effectue une analyse en composantes connexes afin de regrouper les arcs connectés entre eux.
Il est nécessaire d'utiliser cet algorithme pour utiliser l'outil [métriques de connectivité](connectivity_metrics.html)

![](/img/network_analysis/subgraph_creation/interface.png){fig-align="center" width=65%}

## Sorties

Deux nouvelles couches sont générées en sortie, une couche de points et une couche de polylignes. Dans chacune des couches un champ **comp** est ajouté. Il représente l'identifiant unique du réseau de haie. 

![Réseaux de haies](/img/network_analysis/subgraph_creation/output.png){fig-align="center"}
:::

::: {.justify .content-visible when-profile="english"}

# Subgraph creation

## Inputs

This tool takes the linear and point layers and performs a connected components analysis to group connected arcs together. It's necessary to use this algorithm to utilize the [Connectivity Metrics](connectivity_metrics.html) tool.

![](/img/network_analysis/subgraph_creation/interface.png){fig-align="center" width=65%}

## Outputs

Two new layers are generated as output: a point layer and a polyline layer. In each of these layers, a field **comp** is added. It represents the unique identifier of the hedge network.

![Hedge Networks](/img/network_analysis/subgraph_creation/output.png){fig-align="center"}

:::
